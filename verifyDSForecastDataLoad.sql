--HIEU: Verify DS forecast data load to ensure no variance
DECLARE @jFcstStartDate INT = 20160701
DECLARE @jFcstEndDate INT = 20161231

SELECT x.*
FROM
(
SELECT p.[CompanyCode], p.BusinessCode, p.StockCode, p.ShipToCode
,ISNULL(p.[StageDB],0) AS fcstStageDB
,ISNULL(p.[Fact],0) AS fcstFact
,ISNULL(p.[StageDB],0)-ISNULL(p.[Fact],0) AS fcstVariance
FROM
(
	--HIEU: Get 6 months for forecast data from StageDB (as import from file)
	SELECT	'StageDB' AS [Source]
	, 'MCA' AS [CompanyCode]
	, a.Corporation AS BusinessCode
	, a.Item AS StockCode
	, a.[Ship To] AS ShipToCode
	, SUM(a.[Adjusted Forecast 1]+a.[Adjusted Forecast 2]+a.[Adjusted Forecast 3]
		+a.[Adjusted Forecast 4]+a.[Adjusted Forecast 5]+a.[Adjusted Forecast 6]) AS FcstQty
	FROM StageDB.dbo.dsCustomerForecastReport AS a
	WHERE	a.[Ship To] IN ('3CRO','3DDW','9DDW')
	GROUP BY a.Corporation, a.Item, a.[Ship To]
	--HIEU: Get 6 months of alloc forecast data form ProntoMEY (cube data)
	UNION ALL
	SELECT	'Fact' AS [Source]
	, 'MCA' AS [CompanyCode]
	, b.fk_BusinessCode AS BusinessCode
	, a.StockCode AS StockCode
	, a.fk_DSShipToCode AS ShipToCode
	, SUM(a.DistrAdjFcstQty) AS FcstQty
	FROM ProntoMEY.dbo.Fact_SalesForecast AS a INNER JOIN
		ProntoMEY.dbo.Dim_DebMaster AS b ON a.AccountCode = b.AccountCode
	WHERE	a.FcstDateKey BETWEEN @jFcstStartDate AND @jFcstEndDate
	GROUP BY b.fk_BusinessCode, a.StockCode, a.fk_DSShipToCode
) AS SourceTable
PIVOT
(
SUM(FcstQty)
FOR [Source] IN ([StageDB],[Fact])
) AS p
) AS x
WHERE x.fcstVariance <> 0		--variance only

--select * from Pronto.dbo.v_MEY_ZusrDsPriceCost





--CompanyCode	BusinessCode	StockCode	ShipToCode	fcstStageDB	fcstFact	fcstVariance
--MCA	ECOM	841050	3CRO	100	0	100
--MCA	ECOM	168580	3CRO	200	0	200
----MCA	ECOM	463410	3CRO	300	0	300
--SELECT a.*
--	FROM StageDB.dbo.dsCustomerForecastReport AS a
--	WHERE	a.[Ship To] IN ('3CRO','3DDW','9DDW')
--	AND Corporation = 'ECOM'
--	GROUP BY a.Corporation, a.Item, a.[Ship To]




--SELECT	'StageDB' AS [Source]
--	, 'MCA' AS [CompanyCode]
--	, a.Corporation AS BusinessCode
--	, a.Item AS StockCode
--	, a.[Ship To] AS ShipToCode
--	, SUM(a.[Adjusted Forecast 1]+a.[Adjusted Forecast 2]+a.[Adjusted Forecast 3]
--		+a.[Adjusted Forecast 4]+a.[Adjusted Forecast 5]+a.[Adjusted Forecast 6]) AS FcstQty
SELECT SUM(a.[Adjusted Forecast 1]) AS [Adjusted Forecast 1]
,SUM(a.[Adjusted Forecast 2]) AS [Adjusted Forecast 2]
,SUM(a.[Adjusted Forecast 3]) AS [Adjusted Forecast 3]
,SUM(a.[Adjusted Forecast 4]) AS [Adjusted Forecast 4]
,SUM(a.[Adjusted Forecast 5]) AS [Adjusted Forecast 5]
,SUM(a.[Adjusted Forecast 6]) AS [Adjusted Forecast 6]
,SUM(a.[Adjusted Forecast 7]) AS [Adjusted Forecast 7]
,SUM(a.[Adjusted Forecast 8]) AS [Adjusted Forecast 8]
,SUM(a.[Adjusted Forecast 9]) AS [Adjusted Forecast 9]
,SUM(a.[Adjusted Forecast 10]) AS [Adjusted Forecast 10]
,SUM(a.[Adjusted Forecast 11]) AS [Adjusted Forecast 11]
,SUM(a.[Adjusted Forecast 12]) AS [Adjusted Forecast 12]
FROM StageDB.dbo.dsCustomerForecastReport AS a
WHERE	a.[Ship To] IN ('3CRO','3DDW','9DDW')
--GROUP BY a.Corporation, a.Item, a.[Ship To]